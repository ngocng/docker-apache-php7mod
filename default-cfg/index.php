<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome to your website!</title>
</head>
<body>
    <main>
        <h1>Welcome to your website!</h1>

        <label>Current PHP version:</label> <?php echo phpversion();?><br/>
		
        <label>Loaded extensions:</label>
        <ul>
            <?php 
			foreach(get_loaded_extensions() AS $e){ 
				echo '<li>',htmlspecialchars($e),'</li>';
			}?>
        </ul>
    </main>
</body>
</html>