# docker-apache-php7mod
Docker image with Apache + mod_php7 on Alpine linux

## Environment parameters:

 - APACHE_USER - Apache user name, default "www-data" 
 - APACHE_GROUP - Apache user group, default "www-data"
 - APACHE_UID - Apache user UID, default "1000"
 - APACHE_GID - Apache user GID, default "1000"
 