###########################################################
##			Alpine Apache + mod_php7
###########################################################
FROM registry.gitlab.com/plokko/alpine-php7:latest

ENV APACHE_USER=www-data \
	APACHE_GROUP=www-data \
	APACHE_UID=1000 \
	APACHE_GID=1000
	

# Add apache user & group
RUN addgroup -S -g $APACHE_GID www-data \
	&& adduser -D -S -u $APACHE_UID -g $APACHE_GID -h /var/www -s /sbin/nologin -G $APACHE_USER $APACHE_GROUP;
	
## Upgrade & install
RUN apk upgrade --update --no-cache && \
	apk add --no-cache \
		apache2 php7-apache2

# Fix apache run folder & module links
RUN mkdir /run/apache2 \
	&& mkdir -p /usr/local/apache2 \
	&& ln -s /usr/lib/apache2 /usr/local/apache2/modules \
	&& ln -s /var/log/apache2 /usr/local/apache2/logs \
	&& ln -s /run/apache2 /usr/local/apache2/run \
	&& mkdir -p /var/www/htdocs \
	&& rm -R /var/www/localhost \
	&& chown -R $APACHE_GID:$APACHE_UID /var/www/htdocs
	
# Copy default configuration
COPY ./default-cfg/httpd.conf /etc/apache2/httpd.conf
COPY ./default-cfg/default.conf /etc/apache2/conf.d/default.conf

# Copy default index page 
COPY ./default-cfg/index.php /var/www/htdocs

###################
## Remove apk cache
RUN rm -R /var/cache/apk/*

## Redirect log & errors to docker console
RUN ln -sf /dev/stdout /var/log/apache2/access.log \
	ln -sf /dev/stderr /var/log/apache2/error.log 

EXPOSE 80

# Run apache in foreground
CMD ["httpd","-DFOREGROUND"]
